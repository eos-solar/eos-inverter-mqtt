# https://babel.pocoo.org/en/latest/cmdline.html?highlight=config
pybabel extract -F configs/babel.cfg -o i18n/messages.pot --ignore-dirs="env" .
pybabel init -l ca -i i18n/messages.pot -d i18n
pybabel init -l es -i i18n/messages.pot -d i18n
pybabel init -l en -i i18n/messages.pot -d i18n
pybabel update -l ca -i i18n/messages.pot -d i18n
pybabel update -l es -i i18n/messages.pot -d i18n
pybabel update -l en -i i18n/messages.pot -d i18n
poedit
pybabel compile -l ca -i i18n/ca/LC_MESSAGES/messages.po -d i18n
pybabel compile -l es -i i18n/es/LC_MESSAGES/messages.po -d i18n
pybabel compile -l en -i i18n/en/LC_MESSAGES/messages.po -d i18n