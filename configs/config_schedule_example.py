"""
Schedure any register_Map fields in Huawei.py (e.g. 'Model', 'M_P', etc.) or schedule any of the groupings:
    monitor, work, work-extra, alarm, status, info, equipment, optimizers, 3fase
To monitor a group preceed it with 'group:' ("group:<group_name>")

units: m or h (minuts or hours)
fraccions allowed (e.g. 1.5h)
Minimum time 1 minute.
once_at_start: execute once at start of process

example:
'group:monitor': '1.5m' -> read every 90 seconds registers in monitor group
'group:status': '1h -> read every 1 hour registers in status group
'M_A-U': '5m' -> read register 'M_A-U' (grid voltage) every 5 minuts

"""
schedule = {
    'group:monitor': '1m',
    'group:status': '5m',
    'group:work': '1h',
    'group:equipment': 'once_at_start',
    'group:info': '6h',
    'M_A-U': '5m'  # grid voltage
}